function com_google_blogger_b2_gwt_app_BloggerModule() {
    var M = 'bootstrap',
        N = 'begin',
        O = 'com.google.blogger.b2.gwt.app.BloggerModule',
        P = 'startup',
        Q = 'DUMMY',
        R = 0,
        S = 1,
        T = 'iframe',
        U = 'position:absolute; width:0; height:0; border:none; left: -1000px;',
        V = ' top: -1000px;',
        W = 'CSS1Compat',
        X = '<!doctype html>',
        Y = '',
        Z = '<html><head><\/head><body><\/body><\/html>',
        $ = 'undefined',
        _ = 'readystatechange',
        ab = 10,
        bb = 'Chrome',
        cb = 'eval("',
        db = '");',
        eb = 'script',
        fb = 'javascript',
        gb = 'meta',
        hb = 'name',
        ib = 'com.google.blogger.b2.gwt.app.BloggerModule::',
        jb = '::',
        kb = 'gwt:property',
        lb = 'content',
        mb = '=',
        nb = 'gwt:onPropertyErrorFn',
        ob = 'Bad handler "',
        pb = '" for "gwt:onPropertyErrorFn"',
        qb = 'gwt:onLoadErrorFn',
        rb = '" for "gwt:onLoadErrorFn"',
        sb = '#',
        tb = '?',
        ub = '/',
        vb = 'img',
        wb = 'clear.cache.gif',
        xb = 'baseUrl',
        yb = 'com.google.blogger.b2.gwt.app.BloggerModule.nocache.js',
        zb = 'base',
        Ab = '//',
        Bb = 'formfactor',
        Cb = 'desktop',
        Db = 'locale',
        Eb = 'ar',
        Fb = 'user.agent',
        Gb = 'safari',
        Hb = 'loadExternalRefs',
        Ib = 'end',
        Jb = 'http:',
        Kb = 'file:',
        Lb = '_gwt_dummy_',
        Mb = '__gwtDevModeHook:com.google.blogger.b2.gwt.app.BloggerModule',
        Nb = 'Ignoring non-whitelisted Dev Mode URL: ',
        Ob = ':moduleBase',
        Pb = 'head';
    var n = window;
    var o = document;
    p(M, N);

    function p(a, b) {
        if (n.__gwtStatsEvent) {
            n.__gwtStatsEvent({
                moduleName: O,
                sessionId: n.__gwtStatsSessionId,
                subSystem: P,
                evtGroup: a,
                millis: (new Date).getTime(),
                type: b
            })
        }
    }
    com_google_blogger_b2_gwt_app_BloggerModule.__sendStats = p;
    com_google_blogger_b2_gwt_app_BloggerModule.__moduleName = O;
    com_google_blogger_b2_gwt_app_BloggerModule.__errFn = null;
    com_google_blogger_b2_gwt_app_BloggerModule.__moduleBase = Q;
    com_google_blogger_b2_gwt_app_BloggerModule.__softPermutationId = R;
    com_google_blogger_b2_gwt_app_BloggerModule.__computePropValue = null;
    com_google_blogger_b2_gwt_app_BloggerModule.__getPropMap = null;
    com_google_blogger_b2_gwt_app_BloggerModule.__installRunAsyncCode = function() {};
    com_google_blogger_b2_gwt_app_BloggerModule.__gwtStartLoadingFragment = function() {
        return null
    };
    com_google_blogger_b2_gwt_app_BloggerModule.__gwt_isKnownPropertyValue = function() {
        return false
    };
    com_google_blogger_b2_gwt_app_BloggerModule.__gwt_getMetaProperty = function() {
        return null
    };
    var q = null;
    var r = n.__gwt_activeModules = n.__gwt_activeModules || {};
    r[O] = {
        moduleName: O
    };
    com_google_blogger_b2_gwt_app_BloggerModule.__moduleStartupDone = function(e) {
        var f = r[O].bindings;
        r[O].bindings = function() {
            var a = f ? f() : {};
            var b = e[com_google_blogger_b2_gwt_app_BloggerModule.__softPermutationId];
            for (var c = R; c < b.length; c++) {
                var d = b[c];
                a[d[R]] = d[S]
            }
            return a
        }
    };
    var s;

    function t() {
        u();
        return s
    }

    function u() {
        if (s) {
            return
        }
        var a = o.createElement(T);
        a.id = O;
        a.style.cssText = U + V;
        a.tabIndex = -1;
        o.body.appendChild(a);
        s = a.contentWindow.document;
        s.open();
        var b = document.compatMode == W ? X : Y;
        s.write(b + Z);
        s.close()
    }

    function v(k) {
        function l(a) {
            function b() {
                if (typeof o.readyState == $) {
                    return typeof o.body != $ && o.body != null
                }
                return /loaded|complete/.test(o.readyState)
            }
            var c = b();
            if (c) {
                a();
                return
            }

            function d() {
                if (!c) {
                    if (!b()) {
                        return
                    }
                    c = true;
                    a();
                    if (o.removeEventListener) {
                        o.removeEventListener(_, d, false)
                    }
                    if (e) {
                        clearInterval(e)
                    }
                }
            }
            if (o.addEventListener) {
                o.addEventListener(_, d, false)
            }
            var e = setInterval(function() {
                d()
            }, ab)
        }

        function m(c) {
            function d(a, b) {
                a.removeChild(b)
            }
            var e = t();
            var f = e.body;
            var g;
            if (navigator.userAgent.indexOf(bb) > -1 && window.JSON) {
                var h = e.createDocumentFragment();
                h.appendChild(e.createTextNode(cb));
                for (var i = R; i < c.length; i++) {
                    var j = window.JSON.stringify(c[i]);
                    h.appendChild(e.createTextNode(j.substring(S, j.length - S)))
                }
                h.appendChild(e.createTextNode(db));
                g = e.createElement(eb);
                g.language = fb;
                g.appendChild(h);
                f.appendChild(g);
                d(f, g)
            } else {
                for (var i = R; i < c.length; i++) {
                    g = e.createElement(eb);
                    g.language = fb;
                    g.text = c[i];
                    f.appendChild(g);
                    d(f, g)
                }
            }
        }
        com_google_blogger_b2_gwt_app_BloggerModule.onScriptDownloaded = function(a) {
            l(function() {
                m(a)
            })
        }
    }
    com_google_blogger_b2_gwt_app_BloggerModule.__startLoadingFragment = function(a) {
        return B(a)
    };
    com_google_blogger_b2_gwt_app_BloggerModule.__installRunAsyncCode = function(a) {
        var b = t();
        var c = b.body;
        var d = b.createElement(eb);
        d.language = fb;
        d.text = a;
        c.appendChild(d);
        c.removeChild(d)
    };

    function w() {
        var c = {};
        var d;
        var e;
        var f = o.getElementsByTagName(gb);
        for (var g = R, h = f.length; g < h; ++g) {
            var i = f[g],
                j = i.getAttribute(hb),
                k;
            if (j) {
                j = j.replace(ib, Y);
                if (j.indexOf(jb) >= R) {
                    continue
                }
                if (j == kb) {
                    k = i.getAttribute(lb);
                    if (k) {
                        var l, m = k.indexOf(mb);
                        if (m >= R) {
                            j = k.substring(R, m);
                            l = k.substring(m + S)
                        } else {
                            j = k;
                            l = Y
                        }
                        c[j] = l
                    }
                } else if (j == nb) {
                    k = i.getAttribute(lb);
                    if (k) {
                        try {
                            d = eval(k)
                        } catch (a) {
                            alert(ob + k + pb)
                        }
                    }
                } else if (j == qb) {
                    k = i.getAttribute(lb);
                    if (k) {
                        try {
                            e = eval(k)
                        } catch (a) {
                            alert(ob + k + rb)
                        }
                    }
                }
            }
        }
        __gwt_getMetaProperty = function(a) {
            var b = c[a];
            return b == null ? null : b
        };
        q = d;
        com_google_blogger_b2_gwt_app_BloggerModule.__errFn = e
    }

    function A() {
        function e(a) {
            var b = a.lastIndexOf(sb);
            if (b == -1) {
                b = a.length
            }
            var c = a.indexOf(tb);
            if (c == -1) {
                c = a.length
            }
            var d = a.lastIndexOf(ub, Math.min(c, b));
            return d >= R ? a.substring(R, d + S) : Y
        }

        function f(a) {
            if (a.match(/^\w+:\/\//)) {} else {
                var b = o.createElement(vb);
                b.src = a + wb;
                a = e(b.src)
            }
            return a
        }

        function g() {
            var a = __gwt_getMetaProperty(xb);
            if (a != null) {
                return a
            }
            return Y
        }

        function h() {
            var a = o.getElementsByTagName(eb);
            for (var b = R; b < a.length; ++b) {
                if (a[b].src.indexOf(yb) != -1) {
                    return e(a[b].src)
                }
            }
            return Y
        }

        function i() {
            var a = o.getElementsByTagName(zb);
            if (a.length > R) {
                return a[a.length - S].href
            }
            return Y
        }

        function j() {
            var a = o.location;
            return a.href == a.protocol + Ab + a.host + a.pathname + a.search + a.hash
        }
        var k = g();
        if (k == Y) {
            k = h()
        }
        if (k == Y) {
            k = i()
        }
        if (k == Y && j()) {
            k = e(o.location.href)
        }
        k = f(k);
        return k
    }

    function B(a) {
        if (a.match(/^\//)) {
            return a
        }
        if (a.match(/^[a-zA-Z]+:\/\//)) {
            return a
        }
        return com_google_blogger_b2_gwt_app_BloggerModule.__moduleBase + a
    }

    function C() {
        var c = [];

        function d(a) {
            if (a in c) {
                return c[a]
            }
            throw null
        }
        c[Bb] = Cb;
        c[Db] = Eb;
        c[Fb] = Gb;
        __gwt_isKnownPropertyValue = function(a, b) {
            return false
        };
        com_google_blogger_b2_gwt_app_BloggerModule.__computePropValue = d;
        com_google_blogger_b2_gwt_app_BloggerModule.__getPropMap = function() {
            var a = {};
            for (var b in c) {
                a[b] = c[b]
            }
            return a
        };
        n.__gwt_activeModules[O].bindings = com_google_blogger_b2_gwt_app_BloggerModule.__getPropMap;
        com_google_blogger_b2_gwt_app_BloggerModule.__softPermutationId = R;
        return null
    }

    function D() {
        if (!n.__gwt_stylesLoaded) {
            n.__gwt_stylesLoaded = {}
        }
        p(Hb, N);
        p(Hb, Ib)
    }
    w();
    com_google_blogger_b2_gwt_app_BloggerModule.__moduleBase = A();
    r[O].moduleBase = com_google_blogger_b2_gwt_app_BloggerModule.__moduleBase;
    var F = C();
    if (n) {
        var G = !!(n.location.protocol == Jb || n.location.protocol == Kb);
        n.__gwt_activeModules[O].canRedirect = G;

        function H() {
            var b = Lb;
            try {
                n.sessionStorage.setItem(b, b);
                n.sessionStorage.removeItem(b);
                return true
            } catch (a) {
                return false
            }
        }
        if (G && H()) {
            var I = Mb;
            var J = n.sessionStorage[I];
            if (!/^https?:\/\/(localhost|127\.0\.0\.1|[a-zA-Z0-9.-]+\.(corp|prod)\.google\.com):[0-9]+\/.*$/.test(J)) {
                if (J && (window.console && console.log)) {
                    console.log(Nb + J)
                }
                J = Y
            }
            if (J && !n[I]) {
                n[I] = true;
                n[I + Ob] = A();
                var K = o.createElement(eb);
                K.src = J;
                var L = o.getElementsByTagName(Pb)[R];
                L.insertBefore(K, L.firstElementChild || L.children[R]);
                return false
            }
        }
    }
    D();
    p(M, Ib);
    v(F);
    return true
}
com_google_blogger_b2_gwt_app_BloggerModule.succeeded = com_google_blogger_b2_gwt_app_BloggerModule();
if (com_google_blogger_b2_gwt_app_BloggerModule.succeeded) {
    // com_google_blogger_b2_gwt_app_BloggerModule.onScriptDownloaded([""]);
	runCom();
}